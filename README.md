# nic-firmware-audit-T312298

Playbook to audit elastic hosts' NIC firmware, see T312298 for details

## Invoking

"f5" is optional, it dispatches 5 forks at a time, reducing overall strain
on the infrastructure.

``` ansible-playbook -f5 -i elastic.hosts T312298.yml ```
